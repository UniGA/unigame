# UniGame

## Requirements:
- Docker
- Docker-compose

All other packages and dependencies should get installed in the images

## Running the project

Run with `docker-compose up`

In case any of your changes aren't applying, try `docker-compose down &&
docker-compose up --build`

To create and apply migrations use `docker-compose exec web sh` to enter
shell in the web container,
`./manage.py makemigrations unigame` to create any migrations (if you've
changed any models recently) and `./manage.py migrate` to apply all migrations
and inicialize the database

## Superuser creation

`./manage.py createsuperuser --username=Foo --email=foo@bar.com`

You will be prompted for a password which you can then use with the specified
username in the /admin/ section.
