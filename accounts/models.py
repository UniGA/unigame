"""Player models."""
from django.db import models
from django.contrib.auth.models import AbstractUser


class Player(AbstractUser):
    """User model extention."""

    first_name = None
    last_name = None
    max_heroes = models.PositiveIntegerField(default=1)

    def __str__(self):
        """Return formatted __str__."""
        return f"{self.username}"

    @property
    def hero_count(self):
        """Get the players hero count."""
        return self.hero_set.count()

    @property
    def heroes(self):
        """Get the players heroes."""
        return self.hero_set.all()

    @property
    def hero(self):
        """Get the players hero."""
        if self.hero_count == 1:
            return self.heroes[0]

        raise NotImplementedError()

    @property
    def can_create_hero(self):
        """Check whether player can create more heroes or if he has reached his max."""
        return self.hero_count < self.max_heroes
