"""Player account views."""
from django.shortcuts import render
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from .forms import PlayerCreationForm


@login_required
def index(request):
    """View the index page."""
    return render(request, 'accounts/index.html')


def sign_up(request):
    """View or valudate the sign up form."""
    context = {}
    form = PlayerCreationForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            player = form.save()
            login(request, player)
            return render(request, 'accounts/index.html')
    context['form'] = form
    return render(request, 'registration/sign_up.html', context)
