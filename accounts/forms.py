"""Player account forms."""
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Player


class PlayerCreationForm(UserCreationForm):
    """UserCreationForm changes for Player compatibility."""

    class Meta:
        """Metadata."""

        model = Player
        fields = UserCreationForm.Meta.fields

    def save(self, commit=True):
        """Metadata."""
        player = super().save(commit=False)
        if commit:
            player.save()
        return player


class PlayerChangeForm(UserChangeForm):
    """UserChangeForm changes for Player compatibility."""

    class Meta:
        """Metadata."""

        model = Player
        fields = '__all__'
