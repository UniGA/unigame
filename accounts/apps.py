"""Accounts apps file."""
from django.apps import AppConfig


class AccountsConfig(AppConfig):
    """Accounts app config."""

    name = 'accounts'
