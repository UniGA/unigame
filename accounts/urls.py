"""URL Configuration for player accounts."""
from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name="accounts_home"),
    path('accounts/sign_up/', views.sign_up, name="sign-up"),
]
