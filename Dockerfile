FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

RUN apk add gcc musl-dev postgresql-dev

RUN mkdir /code
WORKDIR /code

COPY requirements.txt /code/
RUN pip install -r requirements.txt

COPY . /code/
