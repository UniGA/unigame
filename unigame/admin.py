"""Admin interface settings."""
from django.contrib import admin

from .models import LevelRequirements, HeroClass, Hero

admin.site.register(LevelRequirements)
admin.site.register(HeroClass)
admin.site.register(Hero)
