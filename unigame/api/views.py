"""API Views."""
from unigame.models import Hero, HeroClass
from accounts.models import Player
from rest_framework import viewsets
from unigame.serializers import HeroSerializer, HeroClassSerializer, PlayerSerializer


class HeroViewSet(viewsets.ModelViewSet):
    """API endpoint that allows heroes to be viewed or edited."""
    queryset = Hero.objects.all()
    serializer_class = HeroSerializer


class HeroClassViewSet(viewsets.ModelViewSet):
    """API endpoint that allows heroe classes to be viewed or edited."""
    queryset = HeroClass.objects.all()
    serializer_class = HeroClassSerializer


class PlayerViewSet(viewsets.ModelViewSet):
    """API endpoint that allows heroe classes to be viewed or edited."""
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer
