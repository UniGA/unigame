"""Serializers."""
from rest_framework import serializers
from unigame.models import Hero, HeroClass
from accounts.models import Player


class HeroClassSerializer(serializers.ModelSerializer):
    """Serializer for the HeroClass model."""

    class Meta:
        """Metadata."""

        model = HeroClass
        fields = ('name', 'description')


class PlayerSerializer(serializers.ModelSerializer):
    """Serializer for the Player model."""

    class Meta:
        """Metadata."""

        model = Player
        fields = ('url', 'username', 'email', 'max_heroes')


class HeroSerializer(serializers.ModelSerializer):
    """Serializer for the Hero model."""

    hero_class = HeroClassSerializer()
    owner = PlayerSerializer()

    class Meta:
        """Metadata."""

        model = Hero
        fields = ('name', 'hero_class', 'owner', 'level', 'xp') + Hero.get_attributes()
