"""Hero related forms."""
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _
from unigame.models import Hero


class HeroCreationForm(ModelForm):
    """A form for hero creation."""

    class Meta:
        """Meta information for the hero creation form."""

        model = Hero
        fields = ['name', 'hero_class']
        labels = {
            'name': _('Hero name'),
            'hero_class': _('Hero class'),
        }
