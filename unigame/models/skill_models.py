"""Skills related models."""
from django.db import models


class Skill(models.Model):
    """A model for a skill."""

    name = models.CharField(max_length=64)
    description = models.CharField(max_length=256, blank=True)
    base_damage = models.IntegerField(default=0)
    # The upper limit up to which the damage can vary (in percentage)
    # 100 = no variation, 160 = damage can be 100-160% of base damage
    damage_range = models.IntegerField(default=100)

    def __str__(self):
        """Return formatted __str__."""
        return self.name
