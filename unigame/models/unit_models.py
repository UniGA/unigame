"""Combat Unit model."""
from django.db import models
from unigame.models import Skill


class Unit(models.Model):
    """Combat unit model."""

    name = models.CharField(max_length=64)
    max_hp = models.IntegerField()
    armor = models.IntegerField()
    speed = models.IntegerField()
    skill = models.ForeignKey(Skill, on_delete=models.PROTECT)

    class Meta:
        """Metadata."""
        abstract = True

    def __str__(self):
        """Return formatted __str__."""
        return self.name
