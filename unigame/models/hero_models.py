"""Hero related models."""
from django.db import models
import django.contrib.postgres.fields as postgres_models

from unigame.models import Unit, Skill
from accounts.models import Player


class LevelRequirements(models.Model):
    """
    Model with an array of xp values required for each level.

    Based on the length of the array, we can figure out the maximum level
    """

    array = postgres_models.ArrayField(
        models.PositiveIntegerField()
    )

    def __str__(self):
        """Return formatted __str__."""
        return f"LevelRequirements array: {str(self.array)}"


class HeroClass(models.Model):
    """A model for hero classes."""

    name = models.CharField(max_length=64)
    description = models.CharField(max_length=256, blank=True)
    level_requirements = models.ForeignKey(LevelRequirements, on_delete=models.PROTECT)
    skill = models.ForeignKey(Skill, on_delete=models.PROTECT, null=True)

    base_strength = models.PositiveIntegerField()
    base_agility = models.PositiveIntegerField()
    base_inteligence = models.PositiveIntegerField()
    base_constitution = models.PositiveIntegerField()

    per_level_strength = models.PositiveIntegerField()
    per_level_agility = models.PositiveIntegerField()
    per_level_inteligence = models.PositiveIntegerField()
    per_level_constitution = models.PositiveIntegerField()

    def __str__(self):
        """Return formatted __str__."""
        return self.name


class Hero(Unit):
    """Hero character model."""

    # using the "hero_" prefix because 'class' by itself is a keyword
    hero_class = models.ForeignKey(HeroClass, on_delete=models.CASCADE)
    owner = models.ForeignKey(Player, on_delete=models.CASCADE)
    level = models.IntegerField(default=1)
    _xp = models.IntegerField(default=0)

    strength = models.PositiveIntegerField()
    agility = models.PositiveIntegerField()
    inteligence = models.PositiveIntegerField()
    constitution = models.PositiveIntegerField()

    def __str__(self):
        """Return formatted __str__."""
        return self.name

    def clean(self):
        """Validate model and set default values where needed."""
        # Set all unset attributes to their default
        for attribute in self.attributes:
            if getattr(self, attribute) is None:
                base = self.attribute_base(attribute)
                setattr(self, attribute, base)

    # pylint: disable=W0222
    def save(self, *args, **kwargs):
        """Add a clean call before saving the model."""
        self.full_clean()
        return super(Hero, self).save(*args, **kwargs)

    @property
    def max_level(self):
        """Get the max level based on the size of the LevelRequirements array."""
        return len(self.hero_class.level_requirements.array) + 1

    def attribute_per_level(self, attribute):
        """Get the per level increase of a given attribute."""
        return getattr(self.hero_class, 'per_level_' + attribute)

    def attribute_base(self, attribute):
        """Get the base value of a given attribute."""
        return getattr(self.hero_class, 'base_' + attribute)

    @property
    def is_max_level(self):
        """Check if the hero has reached max level."""
        return self.max_level == self.level

    @property
    def xp_required_to_next_level(self):
        """Return the amount of XP needed to the next level."""
        return self.hero_class.level_requirements.array[self.level - 1]

    @staticmethod
    def get_attributes():
        """Return a tuple of current attribute names."""
        return ('strength', 'agility', 'inteligence', 'constitution')

    @property
    def attributes(self):
        """Return a tuple of current attribute names."""
        return self.get_attributes()

    def level_up(self):
        """Handle the level up process."""
        self.level += 1
        # Increase each attribute by its classes per_level value
        for attribute in self.attributes:
            per_level_increase = self.attribute_per_level(attribute)
            setattr(self, attribute, getattr(self, attribute) + per_level_increase)

    @property
    def xp(self):
        """Get the current xp value."""
        return self._xp

    @xp.setter
    def xp(self, value):
        """Set the xp value and level up if possible."""
        while not self.is_max_level and value >= self.xp_required_to_next_level:
            value -= self.xp_required_to_next_level
            self.level_up()

        self._xp = value

    # Unit attributes override
    @property
    def max_hp(self):
        """Get the max health points."""
        return self.constitution * 10

    @property
    def speed(self):
        """Get the hero's speed."""
        return self.agility

    @property
    def armor(self):
        """Get the hero's armor."""
        return 10

    @property
    def skill(self):
        """Get the hero's combat skill."""
        return self.hero_class.skill
