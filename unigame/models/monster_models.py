"""Monster and Monster related models."""
from django.db import models
from unigame.models import Unit


class Monster(Unit):
    """Monster model."""

    description = models.CharField(max_length=256, blank=True)

    def __str__(self):
        """Return formatted __str__."""
        return self.name
