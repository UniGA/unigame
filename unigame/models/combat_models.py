"""Combat models."""
from django.db import models

from unigame.models import Hero, Monster


class Combat(models.Model):
    """Combat model."""

    date = models.DateTimeField(auto_now_add=True, blank=True)
    result = models.BooleanField(null=True)
    seed = models.IntegerField(null=True)
    heroes = models.ManyToManyField(Hero)
    monsters = models.ManyToManyField(Monster)

    def __str__(self):
        """Return formatted __str__."""
        return f"Combat at {self.date}"

    @property
    def units(self):
        """Get a list of all units in this combat instance."""
        return list(self.heroes.all()) + list(self.monsters.all())
