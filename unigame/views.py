"""Hero related views."""
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from .models import Hero, Monster
from .forms import HeroCreationForm
from .combat import Fight


@login_required
def hero_detail(request, hero_id):
    """Display the details of a single hero."""
    hero = get_object_or_404(Hero, pk=hero_id)
    return render(request, 'hero_detail.html', {'object': hero})


@login_required
def hero_leaderboard(request):
    """List the highest level heroes."""
    heroes = Hero.objects.order_by('-level')
    paginator = Paginator(heroes, 25)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'hero_leaderboard.html', {'page_obj': page_obj})


@login_required
def players_hero_list(request, player_id):
    """List the players heroes."""
    heroes = Hero.objects.filter(owner=player_id)

    return render(request, 'hero_list.html', {'heroes': heroes})


@login_required
def hero_create(request):
    """Hero creation view."""
    player = request.user
    if not player.can_create_hero:
        return my_heroes(request)
    context = {}
    form = HeroCreationForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            hero = form.save(commit=False)
            hero.owner = player
            hero.save()
            return redirect(reverse('hero detail', kwargs={'hero_id': hero.id}))
    context['form'] = form
    return render(request, 'hero_create.html', context)


@login_required
def my_heroes(request):
    """Display the current player's hero / heroes."""
    player = request.user
    if player.hero_count == 0:
        return redirect(reverse('create hero'))
    if player.hero_count == 1:
        return redirect(reverse('hero detail', kwargs={'hero_id': player.hero.id}))
    return players_hero_list(request, player.id)


@login_required
def test_fight(request):
    """Start a fight between the hero and a Wolf."""
    player = request.user
    wolf = Monster.objects.get(pk=1)
    fight = Fight(units=[player.hero, wolf])
    return render(request, 'fight_detail.html', {'fight': fight})
