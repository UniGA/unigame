"""UniGame URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from . import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("accounts.urls")),
    path('accounts/', include('django.contrib.auth.urls')),
    path('hero/', views.my_heroes, name='my heroes'),
    path('hero/<int:hero_id>/', views.hero_detail, name='hero detail'),
    path('leaderboard/hero/', views.hero_leaderboard, name='hero leaderboard'),
    path('create_hero/', views.hero_create, name='create hero'),
    path('test_fight/', views.test_fight, name='test fight'),
    path('api/', include("unigame.api.urls")),
]
