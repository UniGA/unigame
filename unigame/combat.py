"""Combat simulations."""
import sys
import math
import random
from unigame.models import Unit, Combat, Hero, Monster


def is_unit(unit):
    """Check if provided unit has all required attributes."""
    required_attributes = [field.name for field in Unit._meta.get_fields()]
    return all([hasattr(unit, attr) for attr in required_attributes])


class CombatException(Exception):
    """Custom exceptions for the Combat simulation process."""


class Fight:
    """A single combat simulation."""

    class CombatUnit:
        """An internal class used to store temporary Unit information."""
        def __init__(self, unit):
            self.name = unit.name
            self.max_hp = unit.max_hp
            self.hp = unit.max_hp
            self.speed = unit.speed
            self.armor = unit.armor
            self.skill = unit.skill

        @property
        def dead(self):
            """Checks if a unit is dead."""
            return self.hp <= 0

        def __str__(self):
            """Get formatted string."""
            return self.name

    def __init__(self, units=None, combat=None):
        # We want to accept both units and preexisting Combat instances
        if units is not None:
            assert len(units) == 2
            for unit in units:
                assert is_unit(unit)
            self.combat = Combat()
            self.combat.save()
            for unit in units:
                if isinstance(unit, Hero):
                    self.combat.heroes.add(unit)
                elif isinstance(unit, Monster):
                    self.combat.monsters.add(unit)
            self.combat.seed = random.randrange(sys.maxsize)
        elif combat is not None:
            self.combat = combat
        else:
            raise CombatException("Neither a combat instance or units were passed into a Fight")
        self.units = []
        for unit in self.combat.units:
            self.units.append(self.CombatUnit(unit))
        self.rng = random.Random(self.combat.seed)
        self.log = []
        self.process()

    def process(self):
        """Simulate a single fight."""
        turn = 1
        while not self.battle_ended(self.units):
            self.process_turn()
            turn += 1

        if self.combat.result is None:
            # Set result to True in case the player won
            player_won = not self.units[0].dead
            self.combat.result = player_won
            if player_won and isinstance(self.combat.units[0], Hero):
                self.combat.units[0].xp += 10

    def process_turn(self):
        """Simulate a single turn."""
        turn_log = []
        sorted_units = sorted(self.units, key=lambda x: x.speed, reverse=True)
        for current_unit in sorted_units:
            if not current_unit.dead:
                target_options = [unit for unit in sorted_units
                                  if unit is not current_unit and not unit.dead]
                if target_options == []:
                    continue
                target = self.rng.choice(target_options)
                turn_log.append(self.use_skill(current_unit, target))
        self.log.append(turn_log)

    def use_skill(self, unit, target):
        """Simulate a single skill use."""
        skill = unit.skill
        damage = skill.base_damage + math.floor((skill.damage_range - 100) * self.rng.random())
        true_damage = self.calculate_true_damage(target, damage)
        target.hp = target.hp - true_damage
        return (f"{unit} used {skill} on {target}, "
                f"dealing {true_damage}({damage}) damage, "
                f"leaving it at {target.hp} health.")

    @staticmethod
    def calculate_true_damage(target, damage):
        """Calculate true damage done (after armor and such)."""
        return damage - target.armor

    @staticmethod
    def battle_ended(units):
        """Check if the battle is over."""
        return any([unit.dead for unit in units])

    @property
    def logs_printout(self):
        """Return the log in a more printable representation."""
        output = []
        for turn, actions in enumerate(self.log):
            output.append(f"Turn {turn} begins")
            for action in actions:
                output.append(action)
        return "\n".join(output)
