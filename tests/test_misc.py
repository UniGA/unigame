"""Miscellaneous tests."""
from django.test import TransactionTestCase

from unigame.models import LevelRequirements


class FixtureTests(TransactionTestCase):
    """Test provided fixtures."""

    fixtures = ["classes.yaml", "monsters.yaml"]

    def test_loading_fixtures(self):
        """
        Test if fixtures with default data load successfully.

        (fixtures in the fixtures attribute should be loaded if this test even runs)
        """
        # pylint: disable=R0201
        # Loading LevelRequirements model as I believe we'll have one of those
        # in the fixtures at all times (not something you want to create manually)
        # Shouldn't really matter what this "test" contains but whatever
        test = LevelRequirements.objects.get(pk=1)
        test.save()
