"""Test Accounts."""
from django.test import TestCase
from django.contrib.auth import get_user_model


class PlayersManagersTests(TestCase):
    """
    Test account creations.

    We are using the default User Manager for user creation.
    These tests are here to make sure we don't need a custom Manager.
    """

    def test_create_user(self):
        """Test user account creation."""
        user_model = get_user_model()
        user = user_model.objects.create_user(username='user', password='foo')
        self.assertEqual(user.username, 'user')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        # We set first and last name fields to None in the AbstractUser
        self.assertIsNone(user.first_name)
        self.assertIsNone(user.last_name)
        with self.assertRaises(TypeError):
            user_model.objects.create_user()
        with self.assertRaises(ValueError):
            user_model.objects.create_user(username='')

    def test_create_superuser(self):
        """Test superuser account creation."""
        user_model = get_user_model()
        admin_user = user_model.objects.create_superuser('superuser', 'foo')
        self.assertEqual(admin_user.username, 'superuser')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        # We set first and last name fields to None in the AbstractUser
        self.assertIsNone(admin_user.first_name)
        self.assertIsNone(admin_user.last_name)
        with self.assertRaises(ValueError):
            user_model.objects.create_superuser(
                username='superuser', password='foo', is_superuser=False)
