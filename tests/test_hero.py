"""Test hero actions."""
from django.test import TestCase

from unigame.models import LevelRequirements, HeroClass, Hero


class HeroModelTests(TestCase):
    """Test hero model functions."""

    def setUp(self):
        """Set up common objects for tests to use."""
        requirements_array = [10, 20, 30, 40, 50]
        reqs = LevelRequirements(array=requirements_array)

        stats = {'base_strength': 10, 'base_agility': 12, 'base_inteligence': 16,
                 'base_constitution': 12, 'per_level_strength': 1, 'per_level_agility': 4,
                 'per_level_inteligence': 3, 'per_level_constitution': 5}

        hero_class = HeroClass(name="foo", level_requirements=reqs, **stats)

        hero = Hero(name="bar", hero_class=hero_class)

        hero.clean()

        self.requirements_array = requirements_array
        self.hero_class = hero_class
        self.stats = stats
        self.hero = hero

    def test_leveling_up(self):
        """
        Check if hero is leveling up correctly.

        As in levels up when reaching enough XP, stops leveling at the maximum level etc.
        """
        hero = self.hero

        self.assertIs(hero.is_max_level, False)
        self.assertIs(hero.max_level, len(self.requirements_array) + 1)
        self.assertEqual(hero.xp_required_to_next_level, 10)

        hero.xp += 5

        self.assertEqual(hero.xp, 5)
        self.assertEqual(hero.level, 1)

        hero.xp += 5

        self.assertEqual(hero.xp, 0)
        self.assertEqual(hero.level, 2)

        hero.xp += 20 + 30 + 40 + 50

        self.assertEqual(hero.xp, 0)
        self.assertEqual(hero.level, 6)
        self.assertIs(hero.is_max_level, True)

        hero.xp += 69

        self.assertEqual(hero.xp, 69)
        self.assertEqual(hero.level, 6)

    def test_stats(self):
        """Check if hero stats are being set properly at creation and on level up."""
        hero = self.hero
        stats = self.stats

        self.assertEqual(hero.strength, stats['base_strength'])
        self.assertEqual(hero.constitution, stats['base_constitution'])

        hero.xp += hero.xp_required_to_next_level

        self.assertEqual(hero.level, 2)
        self.assertEqual(hero.strength,
                         stats['base_strength'] + stats['per_level_strength'])
        self.assertEqual(hero.constitution,
                         stats['base_constitution'] + stats['per_level_constitution'])

        test_subject = Hero(name="baz", hero_class=self.hero_class, strength=123)
        test_subject.clean()

        self.assertEqual(test_subject.strength, 123)
        self.assertEqual(test_subject.constitution, stats['base_constitution'])
